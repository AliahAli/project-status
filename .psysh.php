<?php
// Temporary fix for issue #34 in laravel tinker where psysh
// tries to run in `/run/user/<uid>/psysh`.
//
// This overrides that behaviour to `.psysh/` inside the project

ini_set('cli.pager', 'less');
return [
    'runtimeDir'	=> './.psysh',
];
