<?php

namespace App\Http\Controllers;

use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index()
    {
        if(auth()->check()) {
            return Inertia::render('Dashboard/Index');
        } else if(env('APP_ENV') == 'prod') {
            return redirect('https://www.webhaus.my/');
        } else {
            return redirect()->route('login');
        }
    }
}
