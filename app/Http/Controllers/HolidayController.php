<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Holiday;
use App\Models\State;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request as Req; 
use Carbon\Carbon;

class HolidayController extends Controller
{
    public function index(Req $request)
    {
        return Inertia::render('Holidays/Index', [
            'filters' => Request::all('search', 'trashed'),
            'holidays' => Holiday::holidayData($request->state_id, $request->holiday_year)
                ->filterSearch($request->search)
                ->with('state')
                ->get()
        ]);
    }

    public function create(Req $request)
    {
        $state = State::find($request);

        return Inertia::render('Holidays/Create', [
            'state' => $state,
        ]);
    }

    public function store()
    {
        Request::validate([
            'holiday_name' => ['required', 'max:50'],
            'holiday_type' => ['required', 'max:50'],
            'date' => ['required'],
        ]);

        Holiday::create([
            'holiday_name' => Request::get('holiday_name'),
            'holiday_type' => Request::get('holiday_type'),
            'holiday_date' => Carbon::parse(Request::get('date'))->timestamp,
            'holiday_year' => Carbon::parse(Request::get('date'))->format('Y'),
            'state_id' => Request::get('state_id'),
        ]);

        return Redirect::route('states')->with('success', 'Holiday created.');
    }

    public function edit(Holiday $holiday)
    {
        $states = State::all();

        return Inertia::render('Holidays/Edit', [
            'holiday' => [
                'id' => $holiday->id,
                'holiday_name' => $holiday->holiday_name,
                'holiday_type' => $holiday->holiday_type,
                'holiday_date' => $holiday->holiday_date,
                'holiday_year' => $holiday->holiday_year,
                'date_formatted' => $holiday->date_formatted,
                'state_id' => $holiday->state_id,
                'deleted_at' => $holiday->deleted_at,
            ],
            'data' => $states
        ]);
    }

    public function update(Holiday $holiday)
    {
         Request::validate([
            'holiday_name' => ['required', 'max:50'],
            'holiday_type' => ['required', 'max:50'],
            'state_id' => ['required'],
            'date_formatted' => ['required'],
         ]);

        $holiday->update([
            'holiday_name' => Request::get('holiday_name'),
            'holiday_type' => Request::get('holiday_type'),
            'holiday_date' => Carbon::parse(Request::get('date_formatted'))->timestamp,
            'holiday_year' => Carbon::parse(Request::get('date_formatted'))->format('Y'),
            'state_id' => Request::get('state_id'),
        ]);

        return Redirect::back()->with('success', 'Holiday updated.');
    }

    public function restore(Holiday $holiday)
    {
        $holiday->restore();

        return Redirect::back()->with('success', 'Holiday restored.');
    }

    public function destroy(Holiday $holiday)
    {
        $holiday->delete();
        return Redirect::back()->with('success', 'Holiday deleted.');
    }
}
