<?php

namespace App\Http\Controllers;

use App\Models\Activity;
use App\Models\Group;
use App\Models\Project;
use App\Models\State;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request as Req;
use Illuminate\Support\Str;
use Illuminate\Validation\Rule;
use Inertia\Inertia;
use Carbon\Carbon;
use DateTime;

class ProjectController extends Controller
{
    protected $name = 'Project';
    protected $viewName;
    protected $routeName;

    public function __construct()
    {
        $this->viewName = Str::plural($this->name);
        $this->routeName = strtolower($this->viewName);
    }

    public function index()
    {
        return Inertia::render($this->viewName . '/Index', [
            'filters' => Request::all('search', 'trashed'),
            'projects' => Project::orderBy('name')
                ->filter(Request::only('search', 'trashed'))
                ->paginate(10)
                ->withQueryString()
                ->through(fn ($project) => [
                    'id' => $project->id,
                    'name' => $project->name,
                    'alias' => $project->alias,
                    'url' => $project->url,
                    'deleted_at' => $project->deleted_at,
                    'groups' => [],
                ]),
        ]);
    }

    public function create()
    {
        $states = State::all();
        return Inertia::render('Projects/Create', [
            'data' => $states
        ]);
    }

    public function store()
    {
        $project = Project::create(
            Request::validate([
                'name' => ['required', 'max:100'],
                'alias' => ['required', 'max:255', Rule::unique('projects')],
                'state_id' => ['required'],
            ])
        );

        return Redirect::route($this->routeName . '.edit', $project)->with('success', $this->name . ' created.');
    }

    public function edit(Project $project, Req $request)
    {
        $states = State::all();
        $groups = Group::groupData($project->id)->get();
        
        return Inertia::render($this->viewName . '/Edit', [
            'project' => [
                'id' => $project->id,
                'name' => $project->name,
                'alias' => $project->alias,
                'url' => $project->url,
                'state_id' => $project->state_id,
                'deleted_at' => $project->deleted_at,
                'groups' => $groups,
            ],
            'data' => $states
        ]);
    }

    public function view(Project $project)
    {
        // $groups = $this->getGroups();
        return Inertia::render('Public/' . $this->viewName . '/View', [
            'project' => [
                'id' => $project->id,
                'name' => $project->name,
                'alias' => $project->alias,
            ],
            'updated_at' => Carbon::createFromFormat('U', 1638802419),
            'groups' => Group::groupData($project->id)->get(),
        ]);
    }

    public function update(Project $project, Req $request)
    {
        $project->update(
            Request::validate([
                'name' => ['required', 'max:100'],
                'alias' => ['required', 'max:255', Rule::unique('projects')->ignore($project->id)],
                'state_id' => ['required'],
            ])
        );

        // get project id
        $id = $project->id;
        Group::where('project_id', $id)->delete();
        $inputs = $request->input('groups', []);
        if (!empty($inputs) && is_array($inputs) ) {
            foreach ($inputs as $key => $input) {
                $task_group = new Group();
                $task_group->number = $key + 1;
                $task_group->title = $input['title'];
                $task_group->project_id = $id;
                $task_group->save();

                foreach ($input['activities'] as $subkey => $activity) {
                    $task = new Activity();
                    $task->number = ($subkey + 1);
                    $task->title = $activity['title'];
                    $task->description = $activity['description'] ?? null;
                    $task->start_at = $activity['start_at_formatted'] ?? null;
                    $task->end_at = $activity['end_at_formatted'] ?? null;
                    $task->completed_at = $activity['completed_at_formatted'] ?? null;
                    $task->percentage = $activity['percentage'] ?? '0';
                    $task->group_id = $task_group->id;
                    $task->save();
                }
            }
        }
        return Redirect::route($this->routeName)->with('success', $this->name . ' updated.');
    }

    public function destroy(Project $project)
    {
        $project->delete();

        return Redirect::back()->with('success', $this->name . ' deleted.');
    }

    public function restore(Project $project)
    {
        $project->restore();

        return Redirect::back()->with('success', $this->name . ' restored.');
    }
}