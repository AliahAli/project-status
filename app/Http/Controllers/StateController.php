<?php

namespace App\Http\Controllers;

use App\Models\Holiday;
use App\Models\State;
use Inertia\Inertia;
use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request as Req;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class StateController extends Controller
{
    public function index(Req $request)
    {
        return Inertia::render('States/Index', [
            'filters' => Request::all('search', 'trashed'),
            'holidays' => Holiday::groupBy('state_id', 'holiday_year')
            ->filterSearch($request->search)
            ->get()
        ]);
    }

    public function create()
    {
        $states = State::all();

        return Inertia::render('States/Create', [
            'data' => $states
        ]);
    }

    public function store(Req $request)
    {
        Request::validate([
            'holiday_name' => ['required', 'max:50'],
            'holiday_type' => ['required', 'max:50'],
            'state_id' => ['required'],
            'date' => ['required'],
        ]);

        $state_ids = $request->input('state_id', []);
        foreach ($state_ids as $state_id) {
            $holiday_date = Carbon::parse($request->date)->timestamp;
            $holiday_year = Carbon::parse($request->date)->format('Y');

            $holiday = new Holiday();
            $holiday->holiday_name = $request->holiday_name;
            $holiday->holiday_type = $request->holiday_type;
            $holiday->holiday_date = $holiday_date;
            $holiday->holiday_year = $holiday_year;
            $holiday->state_id = $state_id;
            $holiday->save();
        }

        return Redirect::route('states')->with('success', 'Holiday created.');
    }


}
