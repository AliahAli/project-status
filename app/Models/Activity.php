<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Activity extends Model
{
    use HasFactory;

    protected $fillable = [
        'number',
        'title',
        'description',
        'start_at',
        'end_at',
        'duration',
        'completed_at',
        'percentage',
    ];

    protected $dateFormat = 'U';

    protected $dates = [
        'start_at',
        'end_at',
        'completed_at',
        'created_at',
        'updated_at'
    ];

    public function scopeActivityData($query, $id)
    {
        return $query->where('group_id', $id);
    }

    protected $appends = [
        'start_formatted',
        'end_formatted',
        'completed_formatted',
        'start_at_formatted',
        'end_at_formatted',
        'completed_at_formatted',
        'display_duration'
    ];

    public function group()
    {
        return $this->belongsTo(Group::class);
    }

    public function getStartFormattedAttribute()
    {
        if(is_null($this->start_at)) return '-';
        return $this->start_at->format('d M Y');
    }

    public function getEndFormattedAttribute()
    {
        if (is_null($this->end_at)) return '-';
        return $this->end_at->format('d M Y');
    }

    public function getCompletedFormattedAttribute()
    {
        if (is_null($this->completed_at)) return '-';
        return $this->completed_at->format('d M Y');
    }

    // Custom attribute for date input value

    public function getStartAtFormattedAttribute()
    {
        if(is_null($this->start_at)) return '';
        return $this->start_at->format('Y-m-d');
    }

    public function getEndAtFormattedAttribute()
    {
        if (is_null($this->end_at)) return '';
        return $this->end_at->format('Y-m-d');
    }

    public function getCompletedAtFormattedAttribute()
    {
        if (is_null($this->completed_at)) return '';
        return $this->completed_at->format('Y-m-d');
    }

    // Custom attribute for display duration
    public function getDisplayDurationAttribute() {
        $date = clone $this->start_at;
        $days = 0;
        $holidays = Holiday::where('state_id', Group::find($this->group_id)->project->state_id)->get();

        while ($date <= $this->end_at) {
            foreach ($holidays as $holiday) {
                if ($date == $holiday->holiday_date) 
                {
                    $date = $date->addDays(1);
                    continue;
                }
            }
            if ($date->isWeekday()) {
                $days++;
            }
            $date = $date->addDays(1);
        }
        return $days;
    }

    
}