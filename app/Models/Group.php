<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    use HasFactory;

    protected $fillable = [
        'number',
        'title',
    ];

    protected $dateFormat = 'U';

    protected $dates = [
        'created_at',
        'updated_at'
    ];

    protected $with=[
        'activities',
        'project'
    ];

    public function scopeGroupData($query, $id)
    {
        return $query->where('project_id', $id);
    }

    public function project()
    {
        return $this->belongsTo(Project::class);
    }

    public function activities() {
        return $this->hasMany(Activity::class);
    }
}
