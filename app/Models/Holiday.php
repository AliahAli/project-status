<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class Holiday extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $dateFormat = 'U';

    protected $dates = ['deleted_at', 'date', 'holiday_date'];

    protected $casts = ['holiday_name', 'holiday_type', 'holiday_year', 'state_id', 'holiday_date', 'date'];

    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where($field ?? 'id', $value)->withTrashed()->firstOrFail();
    }

    public function scopeHolidayData($query, $id, $holiday_year) {
        return $query->where('state_id', $id)->where('holiday_year', $holiday_year)->orderBy('holiday_date');
    }

    public function scopeStateData($query, $id)
    {
        return $query->where('state_id', $id);
    }

    public function scopeFilterSearch($query, $search)
    {
        return $query->where(function ($query2) use ($search) {
            $query2->where('holiday_name', 'like', '%' . $search . '%')
                ->orWhere('holiday_type', 'like', '%' . $search . '%')
                ->orWhere('holiday_year', 'like', '%' . $search . '%')
                ->orWhere('holiday_date', 'like', '%' . $search . '%')
                ->orWhereHas('state', function ($query3) use ($search) {
                    $query3->where('state_name', 'like', '%' . $search . '%');
                });
        });
    }

    protected $holiday;

    protected $with = ['state'];

    protected $appends = [
        'state_name',
        'holiday_date_formatted',
        'date_formatted'
    ];

    public function state()
    {
        return $this->belongsTo(State::class);
    }

    public function getStateNameAttribute()
    {
        return $this->state->state_name;
    }

    public function getHolidayDateFormattedAttribute()
    {
        if (is_null($this->holiday_date)) return '-';
        return $this->holiday_date->format('d M');
    }

    public function getDateFormattedAttribute()
    {
        if (is_null($this->holiday_date)) return '';
        return $this->holiday_date->format('Y-m-d');
    }
}
