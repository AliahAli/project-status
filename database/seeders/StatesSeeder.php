<?php

namespace Database\Seeders;

use App\Models\State;
use Illuminate\Database\Seeder;

class StatesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {  
        $states = [
              ["state_name" => "Johor"],
              ["state_name" => "Kedah"],
              ["state_name" => "Kelantan"],
              ["state_name" => "Melaka"],
              ["state_name" => "Negeri Sembilan"],
              ["state_name" => "Pahang"],
              ["state_name" => "Penang"],
              ["state_name" => "Perak"],
              ["state_name" => "Perlis"],
              ["state_name" => "Sabah"],
              ["state_name" => "Sarawak"],
              ["state_name" => "Selangor"],
              ["state_name" => "Terengganu"],
              ["state_name" => "Kuala Lumpur"],
              ["state_name" => "Labuan"],
              ["state_name" => "Putrajaya"]
        ];

        foreach ($states as $key => $state) {
            State::create($state);
        }

    }
}


