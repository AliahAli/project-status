<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HolidayController;


Route::get('holidays', [HolidayController::class, 'index'])
    ->name('holidays')
    ->middleware('auth');

Route::get('holidays/create', [HolidayController::class, 'create'])
    ->name('holidays.create')
    ->middleware('auth');

Route::post('holidays', [HolidayController::class, 'store'])
    ->name('holidays.store')
    ->middleware('auth');

Route::get('holidays/{holiday}/edit', [HolidayController::class, 'edit'])
    ->name('holidays.edit')
    ->middleware('auth');

Route::put('holidays/{holiday}', [HolidayController::class, 'update'])
    ->name('holidays.update')
    ->middleware('auth');

Route::delete('holidays/{holiday}', [HolidayController::class, 'destroy'])
    ->name('holidays.destroy')
    ->middleware('auth');

Route::put('holidays/{holiday}/restore', [HolidayController::class, 'restore'])
    ->name('holidays.restore')
    ->middleware('auth');
