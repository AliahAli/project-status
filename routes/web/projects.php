<?php

// projects

use App\Http\Controllers\ProjectController;
use Illuminate\Support\Facades\Route;

Route::get('projects', [ProjectController::class, 'index'])
    ->name('projects')
    ->middleware('auth');

Route::get('projects/create', [ProjectController::class, 'create'])
    ->name('projects.create')
    ->middleware('auth');

Route::post('projects', [ProjectController::class, 'store'])
    ->name('projects.store')
    ->middleware('auth');

Route::get('projects/{project}/edit', [ProjectController::class, 'edit'])
    ->name('projects.edit')
    ->middleware('auth');

Route::put('projects/{project}', [ProjectController::class, 'update'])
    ->name('projects.update')
    ->middleware('auth');

Route::get('p/{project:alias}', [ProjectController::class, 'view'])
    ->name('projects.view');

Route::delete('projects/{project}', [ProjectController::class, 'destroy'])
    ->name('projects.destroy')
    ->middleware('auth');

Route::put('projects/{project}/restore', [ProjectController::class, 'restore'])
    ->name('projects.restore')
    ->middleware('auth');
