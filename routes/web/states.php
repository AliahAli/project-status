<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StateController;

Route::get('states', [StateController::class, 'index'])
    ->name('states')
    ->middleware('auth');

Route::get('states/create', [StateController::class, 'create'])
->name('states.create')
->middleware('auth');

Route::post('states', [StateController::class, 'store'])
->name('states.store')
->middleware('auth');
